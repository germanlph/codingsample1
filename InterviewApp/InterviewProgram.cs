﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nbs.Interview
{
    #region Rules and Example Output
    /*
        Computer: What is your name?
        User: Joe
        Computer: Hello Joe!!!
        Computer: How many points do you think the Huskers will score on Saturday?
        Joe: tons
        Computer: I don't understand you.  Please enter a number of points.
        Computer: How many points do you think the Huskers will score on Saturday?
        Joe: 14
        Computer: 14??? I'm thinking they'll need at least 21 to win.  Try again Joe.
        Computer: How many points do you think the Huskers will score on Saturday?
        Joe: 21
        Computer: Odd scores are bad luck.  Please try again Joe.
        Computer: How many points do you think the Huskers will score on Saturday?
        Joe: 28
        Computer: 28 sounds pretty good!  GO BIG RED!

        Summary Requirements: 
        - Must provide a valid number input
        - Minimum 21 points to win
        - Odd points are not accepted
    */
    #endregion

    class InterviewProgram
    {
        static void Main(string[] args)
        {
            string name = Prompt("What is your name?", "User");
            Print("Hello {0}!!!", name);

            //Your Code Here
            ErrorType errorType = ErrorType.None;
            do
            {
                string guess = Prompt("How many points do you think the Huskers will score on Saturday?", name);
                errorType = ValidateGuess(guess);
                switch (errorType) {
                    case ErrorType.NotANumber:
                        Print("I don't understand you.  Please enter a number of points.");
                        break;
                    case ErrorType.Low:
                        Print("{0}??? I'm thinking they'll need at least 21 to win.  Try again {1}.", new string[] { guess, name });
                        break;
                    case ErrorType.Odd:
                        Print("Odd scores are bad luck.  Please try again {0}.", new string[] { name });
                        break;
                    case ErrorType.None:
                        Print("{0} sounds pretty good!  GO BIG RED!", new string[] { guess });
                        break;
                    default:
                        break;
                }
            } while (errorType != ErrorType.None);

            EndProgram();
        }

        //Asks a question and receives input from the user
        private static string Prompt(string promptMessage, string userPrompt)
        {
            Print(promptMessage);
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write(string.Format("{0}: ", userPrompt));
            var input = Console.ReadLine();
            Console.ResetColor();
            return input;
        }

        //Prints output to the screen
        private static void Print(string printMessage)
        {
            Print(printMessage, new object[] { });
        }

        //Prints formatted output to the screen
        private static void Print(string printMessage, params object[] args)
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("Computer: " + string.Format(printMessage, args));
            Console.ResetColor();
        }

        private static void EndProgram()
        {
            if (System.Diagnostics.Debugger.IsAttached)
            {
                Console.Write("Press any key to continue . . . ");
                Console.ReadKey();
            }
        }

        private static ErrorType ValidateGuess(string guess)
        {
            if (!int.TryParse(guess, out int points)) {
                return ErrorType.NotANumber;
            }
            if (points < 21)
            {
                return ErrorType.Low;
            }
            if (points % 2 > 0)
            {
                return ErrorType.Odd;
            }
            return ErrorType.None;
        }
    }

    internal enum ErrorType
    {
        None,
        NotANumber,
        Low,
        Odd
    }
}
